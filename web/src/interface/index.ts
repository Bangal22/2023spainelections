export interface PartyTweetsCounter {
    'class_name': string,
    'recount': number,
    'color': string,
    'border_color': string
}

export interface SentimentCount {
    sentiment_type: string,
    recount: number
}

export interface SumRetweetsSentiment {
    'sentiment_type': string,
    'recount': number,
}

export interface RetweetSumPoliticalParties {
    'class_name': string,
    'recount': number
}

export interface PoliticalParties {
    name: string,
    title: string,
    png: string,
    height: number,
    width: number,
    link: string,
    alt: string
}

export interface InfoByPoliticalParties {
    class_name: string,
    total_tweets: number,
    total_retweets: number,
    positive_tweets: number,
    negative_tweets: number,
    neutral_tweets: number
}

export interface BarConfig {
    labels: string[],
    values: number[],
    title?: string,
    color?: string[] | string,
    bColor?: string[] | string,
    axis?: "x" | "y" | undefined
}

export interface PieConfig {
    labels: string[],
    values: number[],
    title?: string,
    color?: string[] | string,
    bColor?: string[] | string,
    axis?: "x" | "y" | undefined
}

export interface Representative {
    name: string, 
    label: string, 
    img : string, 
    width: string, 
    height: string
}

export interface TotalTweets {
    total_tweets: number
}

export interface TotalRetweets {
    total_retweets : number
}

export type Positive = {
    positive: string
}

export type Negative = {
    negative: string
}

export type Neutral = {
    neutral: string
}
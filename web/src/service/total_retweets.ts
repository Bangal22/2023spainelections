import type { TotalRetweets } from '@interface/index'
import { variables } from 'src/variables'

export async function getTotalRetweets() : Promise<TotalRetweets> {
    const response = await fetch(`${variables.API}/total-retweets`);
    const data: TotalRetweets = await response.json()
    return data
}
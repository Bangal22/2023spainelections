import type { SentimentCount } from '@interface/index'
import { variables } from 'src/variables'
export async function getSentimentCount(): Promise<SentimentCount[]> {
    const response = await fetch(`${variables.API}/recount-sentiment`)
    const data: SentimentCount[] = await response.json()
    return data
}
import type { PartyTweetsCounter } from '@interface/index'
import { variables } from 'src/variables'
export async function getPartyTweetsCounter(): Promise<PartyTweetsCounter[]> {
    const response = await fetch(`${variables.API}/recount-political-parties`)
    const data: PartyTweetsCounter[] = await response.json()
    return data
}
export const sentimentColor: any = {
    positive: '#E66C37cb',
    neutral: '#12239Ecb',
    negative: '#118DFFcb'
}

export const sentimentTextColorGradient: any = {
    positive: 'linear-gradient(rgba(229, 107, 55, 0.849), rgb(226, 131, 90));',
    neutral: 'linear-gradient(rgba(18, 34, 158, 0.784), rgba(75, 85, 157, 0.827));',
    negative: 'linear-gradient(rgb(17, 141, 255), rgb(113, 186, 255));'
}
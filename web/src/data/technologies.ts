interface Technology {
    alt: string,
    svg: string
}
export const technologies : Technology[] = [
    {
        alt: 'SVG del logo de azure',
        svg: 'assets/svg/azure.svg'
    },
    {
        alt: 'SVG del logo de nifi',
        svg: 'assets/svg/nifi.svg'
    },
    {
        alt: 'SVG del logo de railway',
        svg: 'assets/svg/railway.svg'
    },
    {
        alt: 'SVG del logo de docker',
        svg: 'assets/svg/docker.svg'
    },
    {
        alt: 'SVG del logo de astro',
        svg: 'assets/svg/astro.svg'
    },
    {
        alt: 'SVG del logo de node',
        svg: 'assets/svg/node.svg'
    }, 
    {
        alt: 'SVG del logo de chart.js',
        svg: 'assets/svg/chartjs.svg'
    }, 
    {
        alt: 'SVG del logo de express.js',
        svg: 'assets/svg/express.svg'
    }
]
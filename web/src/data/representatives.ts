import type { Representative } from '@interface/index'

export const representatives : Representative[] = [
    {
        name : "Pedro Sánchez", 
        label : "Pedro Sánchez, presidente del Partido Socialista Obrero Español",
        img : "assets/webp/pedro.webp",
        width: "712", 
        height: "400"
        
    }, 
    {
        name : "Yolanda Díaz", 
        label : "Yolanda Díaz, representante del partido de Unidas Podemos",
        img : "assets/webp/yolanda.webp",
        width: "600", 
        height: "400"
    },
    {
        name : "Alberto Núñez Feijóo", 
        label : "Alberto Núñez Feijóo, presidente del Partido Popular",
        img : "assets/webp/feijoo.webp",
        width: "649", 
        height: "400"
    }, 
    {
        name : "Santiago Abascal", 
        label : "Santiago Abascal, Presidente del partido político VOX",
        img : "assets/webp/abascal.webp",
        width: "710", 
        height: "400"
    }, 
]
import type { PoliticalParties } from "src/interface/index";

export const politicalParties : PoliticalParties[] = [
    {
        name: "psoe",
        title: "Partido socialista obreo español",
        png: "assets/webp/psoe.webp",
        height: 100,
        width: 100,
        link: "https://www.psoe.es",
        alt : "Logo del Partido socialista obreo español"
    },
    {
        name: "unidas podemos",
        title: "Unidas podemos",
        png: "assets/webp/podemos.webp",
        height: 100,
        width: 100,
        link: "https://podemos.info/",
        alt : "Logo del partido político de Unidas Podemos"
    },
    {
        name: "pp",
        title: "Partido popular",
        png: "assets/webp/pp.webp",
        height: 100,
        width: 100,
        link: "https://www.pp.es/",
        alt : "Logo del Partido Pouplar"
    },
    {
        name: "vox",
        title: "Vox",
        png: "assets/webp/vox.webp",
        height: 100,
        width: 100,
        link: "https://www.voxespana.es/",
        alt : "Logo del partido político VOX"
    },
];
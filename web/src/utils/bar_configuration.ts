import type { ChartConfiguration } from "chart.js";
import type { BarConfig } from "@interface/index";

export const barConfiguration = ({
  labels,
  values,
  title = "",
  color = "#82CDFF",
  bColor = "#059BFF",
  axis = "x"
}: BarConfig) => {
  const config: ChartConfiguration<'bar'> = {
    type: "bar",
    data: {
      labels: labels,
      datasets: [
        {
          // label: 'Something2',
          data: values,
          backgroundColor: color,
          borderColor: bColor,
          borderWidth: 2,
          borderRadius: 15,
        },
      ],
    },
    options: {
      indexAxis: axis,
      maintainAspectRatio: true, //Poner a falso mas adelante para que ocupe todo el height 
      elements: {
        bar: {
          borderWidth: 2,
        },
      },
      responsive: true,
      plugins: {
        legend: {
          display: false,
          position: "top",
          onHover: handleHover,
          onLeave: handleLeave
        },
        title: {
          display: true,
          text: title,
        },
      },
    },
  };
  return config
};

function handleHover(evt :any , item : any, legend : any) {
  legend.chart.data.datasets[0].backgroundColor.forEach((color : any, index : any, colors : any)  => {
    colors[index] = index === item.index || color.length === 9 ? color : color + '4D';
  });
  legend.chart.update();
}

function handleLeave(evt : any, item : any, legend : any) {
  legend.chart.data.datasets[0].backgroundColor.forEach((color : any, index: any, colors : any) => {
    colors[index] = color.length === 9 ? color.slice(0, -2) : color;
  });
  legend.chart.update();
}
export { barConfiguration } from './bar_configuration'
export { pieConfiguration } from './pie_configuration'
export { setPoint } from './set_point_in_number'

export type Label = string;
export type Value = number;
export type Color = string;
export type BorderColor = string;
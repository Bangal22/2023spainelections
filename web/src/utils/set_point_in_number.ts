export const setPoint = (n: number): string => {
    let numStr = String(n);
    let numLen = numStr.length;
    let numPoints = Math.floor((numLen - 1) / 3);
    let numWithPoints = "";
    for (let i = 0; i < numPoints; i++) {
        let start = numLen - (i + 1) * 3;
        let end = numLen - i * 3;
        numWithPoints = "." + numStr.substring(start, end) + numWithPoints;
    }
    numWithPoints = numStr.substring(0, numLen - numPoints * 3) + numWithPoints;
    return numWithPoints;
}
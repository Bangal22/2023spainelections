import { connection } from '../db/connection/connection.js'
import { pool } from '../db/connection/connection_postgreSQL.js'
import { STATUS_OK, STATUS_ERROR } from '../utils/index.js'


export async function modelRetweetsSumPoliticalParties() {
    try {
        const query = ` SELECT class as class_name, SUM(retweet) as recount 
                        FROM tweets_tab 
                        GROUP BY class `
        const { rows: response } = await pool.query(query) //postgresSQL
        // const response = (await connection()).query(query)
        return { 'status': STATUS_OK, response }
    } catch (err) {
        console.log(err)
        return { 'status': STATUS_ERROR, 'response': [] }
    }
}
import { connection } from '../db/connection/connection.js'
import { client as connectionPostgreSQL} from '../db/connection/connection_postgreSQL.js'

// router.get('/csv', async () => {
    const querySQLServer = ` SELECT * FROM tweets_tab`
    const response = (await connection()).query(querySQLServer)
    const { recordset } = await response
    const postgreSql = await connectionPostgreSQL()
    await postgreSql.connect()
    
    let queryPostgreSQL = `INSERT INTO tweets_tab(tweet_id, class, text, created_at, likes, retweet, language, sentiment_score, sentiment_type) VALUES`
    recordset.forEach(({ id, class: class_name, text, created_at, likes, retweet, language, sentiment_score, sentiment_type }) => {
        const txt = text.replace(/'/g, "''").replace(/\\/g, "\\\\")
        queryPostgreSQL += `('${id}', '${class_name}', '${txt}', '${created_at}', '${likes}', '${retweet}', '${language}', '${sentiment_score}', '${sentiment_type}'),`
    })
    queryPostgreSQL = queryPostgreSQL.replace(/,$/, ";")
    postgreSql.query(queryPostgreSQL, (err, res) => {
        if (err) {
            console.log(err.message)
        }else{
            console.log(res.rows)
        }
    })
    // await postgreSql.end()
// })
import { modelTotalRetweets } from '#model/total_retweets.model.js'
import { HTTP_OK, HTTP_ERROR } from '#utils/index.js'

export async function controllerTotalRetweets(req, res) {
    const { status, response } = await modelTotalRetweets()
    const data = await response
    return status
        // ? res.status(HTTP_OK).json(data.recordset[0])
        ? res.status(HTTP_OK).json(data[0])
        : res.status(HTTP_ERROR).json({ 'message': 'Internal server error when obtaining data', data })

} 
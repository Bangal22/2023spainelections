import pg from 'pg'

// .env 
import dotenv from 'dotenv'
import { fileURLToPath } from 'node:url'
import path from 'node:path'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
dotenv.config({path: `${__dirname}../../../../.env`})

//config
const Pool = pg.Pool
export const pool = new Pool({
    host: process.env.PGHOST,
    port: process.env.PGPORT,
    user: process.env.PGUSER,
    password: process.env.PGPASSWORD,
    database: process.env.PGDATABASE,
    ssl: {
        rejectUnauthorized: false
    }
})


export async function connectionPostgreSQL () {
    return client
}
